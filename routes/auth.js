const express = require('express');
const  authController = require('../controllers/auth');

const router = express.Router();

router.get('/register', (req, res) => {
    res.sendFile('register.html', { root: "./components" });
});

router.post('/register', authController.register);

router.get('/login', (req, res) => {
    res.sendFile('login.html', { root: "./components" })
})
router.post('/login', authController.login );
router.get('/users', (req, res) => {
    res.sendFile('users.html', { root: "./components" })
})
router.get('/getUsers', authController.getUsers);

module.exports = router;
