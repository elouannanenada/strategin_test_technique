require('dotenv').config();
const User = require('../models/user');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const JWT_SECRET = process.env.JWT_SECRET || 'default_secret_key';

//récupérer les données des utilisateurs pour créer un compte
exports.register = async (req, res) => {
  const { email, password } = req.body;

  try {
    const existingUser = await User.findOne({ email });
    if (existingUser) {
      return res.status(400).json({ message: "Il existe déjà un compte avec cet email" });
    }

    const user = new User({ email, password });
    await user.save();

    res.status(201).json({ message: "Compte créé avec succès !" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Erreur lors de la création de compte" });
  }
};


//Verifier les informations de connexion de l'utilisateur et générer un token JWT
exports.login = async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = await User.findOne({ email });
    if (!user) {
      return res.status(401).json({ message: 'Email incorrect' });
    }

    const isValid = await bcrypt.compare(password, user.password);

    if (!isValid) {
      return res.status(401).json({ message: 'Mot de passe incorrect' });
    }
    // Générer le token JWT
    const token = jwt.sign({ userId: user._id }, JWT_SECRET);

    // Send the JWT token back to the client-side
    res.status(200).json({ token });

  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Une erreur est survenue lors de la connexion' });
  }
};


// Récupérer tous les utilisateurs enregistrés dans la bd
exports.getUsers = async (req, res, next) => {
  try {

    if (!req.headers.authorization) {
      return res.status(401).json({ message: "Unauthorized" });
    }

    const token = req.headers.authorization.split(' ')[1];
    if (!token) {
      return res.status(401).json({ message: "Unauthorized" });
    }

    const decodedToken = jwt.verify(token, JWT_SECRET);
    const userId = decodedToken.userId;

    const user = await User.findById(userId);
    if (!user) {
      return res.status(401).json({ message: "Unauthorized" });
    }

    const users = await User.find({}, { email: 1, _id: 0 });
    res.json(users);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};


