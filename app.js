const express = require('express');
const mongoose = require('mongoose');
const authRoutes = require('./routes/auth');
const cors = require('cors');

const app = express();
const PORT = 3000;

app.use(express.json());
app.use(cors());

mongoose.connect('mongodb://127.0.0.1/users', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'Erreur de connexion à MongoDb'));
db.once('open', () => {
    console.log('Connecté à la base de données MongoDB')
});

app.use('/', authRoutes);

app.get('/', (req, res) => {
    res.redirect('/register');
});

app.listen(PORT, () => {
    console.log(`Serveur lancé sur le port ${PORT}`);
});